package edu.upenn.cis.cis455.storage;

import java.math.BigInteger;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class HashWrapper {
	@PrimaryKey
	BigInteger id;
	
	public HashWrapper() {
		id = null;
	}
	
	public HashWrapper(BigInteger id) {
		this.id = id;
	}
}
