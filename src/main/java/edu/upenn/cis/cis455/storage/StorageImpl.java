package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.Transaction;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

import edu.upenn.cis.cis455.crawler.WebInterface;

public class StorageImpl implements StorageInterface {
	final static Logger logger = LogManager.getLogger(StorageImpl.class);
	
	String directory;
	EntityStore dbUser;
	EntityStore dbFile;
	EntityStore dbSeen;
	Environment env;
	
	PrimaryIndex<String, Wrapper> userAccess; 
	PrimaryIndex<String, Wrapper> fileAccess;
	PrimaryIndex<BigInteger, HashWrapper> contentSeen;
		
	static int docCount;
	static int userCount;
	
	public StorageImpl(String directory) throws DatabaseException {
		this.directory = directory;
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        env = new Environment(new File(directory), envConfig);
 
        StoreConfig storeConfig = new StoreConfig();
        storeConfig.setAllowCreate(true);
        storeConfig.setTransactional(true);
        dbUser = new EntityStore(env, "dbUser", storeConfig);
        dbFile = new EntityStore(env, "dbFile", storeConfig);
        dbSeen = new EntityStore(env, "dbSeen", storeConfig);
        userAccess = dbUser.getPrimaryIndex(String.class, Wrapper.class);
        fileAccess = dbFile.getPrimaryIndex(String.class, Wrapper.class);
        contentSeen = dbSeen.getPrimaryIndex(BigInteger.class, HashWrapper.class); 
        docCount = -1;
        userCount = -1;
        
//        dbFile.truncateClass(Wrapper.class);
//		dbSeen.truncateClass(HashWrapper.class);
	}
	
	@Override
	public int getCorpusSize() {
		return (int) fileAccess.count();
	}

	@Override
	public int addDocument(String url, String documentContents) {
		Transaction txn = env.beginTransaction(null, null);
		Transaction txn2 = env.beginTransaction(null, null);
		try {
			
			MessageDigest digest = MessageDigest.getInstance("MD5");			
			byte[] hashedDoc = digest.digest(documentContents.getBytes(StandardCharsets.UTF_8));
			if (fileAccess.contains(url)) {
				txn.commit();
				txn2.commit();
				return -2;
			}
			if (!contentSeen.contains(new BigInteger(hashedDoc))) {
				fileAccess.put(new Wrapper(url, documentContents, docCount));
				txn.commit();
				logger.debug("Trying to add to BerkleyDB, url: " + url);
				logger.debug("Corpus size is now: " + getCorpusSize());
				docCount++;
				
				contentSeen.put(new HashWrapper(new BigInteger(hashedDoc))); 
				txn2.commit();
			return docCount++;
			}
		} catch (NoSuchAlgorithmException e) {
			txn.abort();
			txn2.abort();
			e.printStackTrace();
		}
		txn.commit();
		txn2.commit();
		return -1;
	}

	@Override
	public String getDocument(String url) {
		Wrapper w = fileAccess.get(url);
		if (w != null) {
			return w.valueStr;
		}
		return null;
	}

	@Override
	public int addUser(String username, String password) {
		Transaction txn = env.beginTransaction(null, null);
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hashedPass = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			
			userAccess.put(new Wrapper(username, hashedPass));
			txn.commit();
		} catch (NoSuchAlgorithmException e) {
			txn.abort();
			e.printStackTrace();
		}
		
		return userCount++;
	}

	@Override
	public boolean getSessionForUser(String username, String password) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hashedPass = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			Wrapper w = userAccess.get(username);
			logger.debug("Trying to get a username");
			if (w == null) {
				logger.debug("No Wrapper with that username");
			} else {
				logger.debug("Retrieved with username " + w.key); 
			}
			if (w != null && username.equals(w.key) && java.util.Arrays.equals(hashedPass, (w.value))) {
				return true;
			}
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public void close() {
		logger.debug("Closing databases");
		dbUser.close();		
		dbFile.close();	
		dbSeen.close();
		env.close();
	}

}
