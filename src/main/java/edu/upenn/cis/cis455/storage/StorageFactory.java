package edu.upenn.cis.cis455.storage;

public class StorageFactory {
	
	static StorageInterface s = null;
    public static StorageInterface getDatabaseInstance(String directory) {
        if (s == null) {
        	s = new StorageImpl(directory);
        }
        return s;
    }
}
