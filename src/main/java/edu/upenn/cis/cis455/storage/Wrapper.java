package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class Wrapper {
	@PrimaryKey
	String key;
	byte[] value;
	
	String valueStr;
	int docID;
	
	public Wrapper() {
		key = null;
		value = null;
		valueStr = null;
		docID = 0;
	}
	
	public Wrapper(String key, byte[] value) {
		this.key = key;
		this.value = value;
	}
	
	public Wrapper(String key, String value, int id) {
		this.key = key;
		this.valueStr = value;
		this.value = value.getBytes();
		this.docID = id; 
	}
}
