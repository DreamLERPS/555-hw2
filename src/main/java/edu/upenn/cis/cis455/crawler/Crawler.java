package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageImpl;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class Crawler implements CrawlMaster {
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.
	final static Logger logger = LogManager.getLogger(Crawler.class);
	
    static final int NUM_WORKERS = 10;
    boolean working;
    boolean threadExit;
    
    Integer maxCount;
    Integer curCount;
    Integer maxSize;
    Integer maxConsidered;
    
    StorageInterface db;
    public LinkedList<String> queue;
    HashSet<String> seen;
    HashMap<String, Integer> accessTime; 
    HashMap<String, ArrayList<String>> disallowedPaths;
    HashMap<String, Integer> crawlDelays;

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
      	this.db = db;
    	this.maxSize = size * 1024 * 1024;
    	this.maxCount = count;
    	this.maxConsidered = 0;
    	curCount = 0;   
    	
    	working = false;
    	threadExit = false;
    	
    	queue = new LinkedList<String>();
    	queue.add(startUrl);
    	
    	seen = new HashSet<String>();
    	accessTime = new HashMap<String, Integer>();
    	disallowedPaths = new HashMap<String, ArrayList<String>>();
    	crawlDelays = new HashMap<String, Integer>();
    }

    /**
     * Main thread
     */
    public void start() {
    	logger.debug("Crawler has begun");
    	try {
	    	while(!isDone() && !queue.isEmpty()) {
	    		URLInfo url = new URLInfo(queue.poll());
	    		if (!robotCheck(url.toString())) {
	    			logger.debug("Disallowed from " + url.toString());
	    			continue; 
	    		}
	    		if (accessTime.get(url.getHostName()) != null) {
	    			//logger.debug("Checking last access for known url");
	    			//logger.debug("Current time: " + (int)System.currentTimeMillis() + " can start at " + accessTime.get(url.getHostName()));
		    		if (accessTime.get(url.getHostName()) > (int) System.currentTimeMillis()) {
		    			queue.add(url.toString());
		    			//logger.debug("Current time: " + (int)System.currentTimeMillis() + " can start at " + accessTime.get(url.getHostName()));
		    			continue;
		    		}
	    		}
	    		logger.debug("Tyring to add: " + url.toString());
	    		if (!seen.contains(url.toString())) {
	    			seen.add(url.toString());
	    			URL urlObj = new URL(url.toString());
					HttpURLConnection con = (HttpURLConnection) urlObj.openConnection(); 
					con.setRequestMethod("HEAD");
					
					boolean validLink = true;
					String contentType;
					int contentLength;
					try {
						//Making sure that the link is not null
						InputStream in = con.getInputStream();
						contentType = con.getContentType();
						contentLength = con.getContentLength();
					} catch (FileNotFoundException e) {
						validLink = false;
						contentType = "";
						contentLength = 0;
					}

					logger.debug("Have document with contentType: " + contentType + " -and contentLength: " + contentLength);
					//Checking if we want to parse this
					if (contentLength <= maxSize && 
					   (contentType.equals("text/html") || 
					    contentType.equals("text/xml") ||
						contentType.equals("application/xml") ||
						contentType.endsWith("+xml")) &&
					    validLink) {
						logger.debug("Agreeable content type and length for url: " + url.toString());
						URL urlObjGet = new URL(url.toString());
						HttpURLConnection conGet = (HttpURLConnection) urlObjGet.openConnection(); 
						conGet.setRequestMethod("GET");
						InputStream inGet = conGet.getInputStream();
						accessTime.put(url.getHostName(), -1);
						
						Scanner s = new Scanner(inGet).useDelimiter("\\A");
						String result = s.hasNext() ? s.next() : "";
						//logger.debug("Result: " + result);
						int resolvedID = db.addDocument(url.toString(), result);
						if (resolvedID != -1) {
							if (resolvedID >= 0) {
								curCount++;
							}
							logger.debug("Added " + url.toString() + " and contents to database");
							if (contentType.equals("text/html")) {
								getLinks(url);
							} else {
								logger.debug("Content not HTML");
							}
						} else {
							logger.debug(url.toString() + " already in database or some error occured");
						} 
					}
	    		} else {
	    			getLinks(url);
	    		}
	    	}
    	} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	db.close();
    }
    
    public void getLinks(URLInfo url) throws IOException {
    	Document document = Jsoup.connect(url.toString()).get();
		Elements linksOnPage = document.select("a[href]");
		for (Element e: linksOnPage) {
			String newLink = e.attr("abs:href");
			logger.debug("Adding new link: " + newLink);
			queue.add(e.attr("abs:href"));
		}
    }
    
    public boolean robotCheck(String url) throws IOException {
    	
    	URLInfo urlInfo = new URLInfo(url);
    	if (disallowedPaths.get(urlInfo.getHostName()) == null) {
	    	String urlRobot = (urlInfo.isSecure() ? "https://" : "http://") + urlInfo.getHostName() + "/robots.txt";
	    	logger.debug("Using robots.txt link: " + urlRobot);
	    	URL urlObj = new URL(urlRobot);
			HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
			con.setRequestMethod("GET");
			InputStream inGet = con.getInputStream();
			
			Scanner s = new Scanner(inGet).useDelimiter("\\A");
			String result = s.hasNext() ? s.next() : "";
			
			String[] lines = result.split("\n");
			//logger.debug("Result: " + result);
			ArrayList<String[]> tupleList = new ArrayList<String[]>();
			for (String str: lines) {
				String[] tup = str.split(":");
				tupleList.add(tup);
			}
			ArrayList<String[]> pairList = new ArrayList<String[]>();
			for (String[] strList: tupleList) {
				if (strList.length == 2) {
					pairList.add(strList);
				}
			}
			int startingIndex = -1;
			
			String[] names = {"*", "cis455crawler"};
			for (String name: names) {
				for (int i = 0; i < pairList.size(); i++) {
					if (pairList.get(i)[0].contains("User-agent") && pairList.get(i)[1].contains(name)) {
						startingIndex = i+1;
						break;
					}
				}
			}
			ArrayList<String> disallowed = new ArrayList<String>();
			int crawlDelay = 0;
			while (!pairList.get(startingIndex)[0].contains("User-agent")) {
				if (pairList.get(startingIndex)[0].contains("Disallow")) {
					disallowed.add(pairList.get(startingIndex)[1].trim());
				} else if (pairList.get(startingIndex)[0].contains("Crawl-delay")) {
					crawlDelay = Integer.parseInt(pairList.get(startingIndex)[1].trim());
					
				}
				startingIndex++;
			}	
			disallowedPaths.put(urlInfo.getHostName(), disallowed);
			crawlDelays.put(urlInfo.getHostName(), crawlDelay);

			//logger.debug("Crawl delay is: " + crawlDelay);
			
    	}
    	if (accessTime.get(urlInfo.getHostName()) == null) {
			accessTime.put(urlInfo.getHostName(), -1);
		}
		if (accessTime.get(urlInfo.getHostName()) < 0) {
			logger.debug("Setting next access time of " + ((int)System.currentTimeMillis() + crawlDelays.get(urlInfo.getHostName()) * 1000));
			accessTime.put(urlInfo.getHostName(), (int) System.currentTimeMillis() + crawlDelays.get(urlInfo.getHostName()) * 1000);
		}
		for (String str: disallowedPaths.get(urlInfo.getHostName())) {
			if (urlInfo.getFilePath().contains(str)) {
				return false;
			}
		}
		
		return true;
    }
    
    
    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
    	curCount++; 
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
    	if (curCount >= maxCount) {
    		return true;
    	}
        return false;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    	this.working = working;
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    	threadExit = true;
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.TRACE);
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);

        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();

        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        // TODO: final shutdown

        System.out.println("Done crawling!");
        
    }

}
