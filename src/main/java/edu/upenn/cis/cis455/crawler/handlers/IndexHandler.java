package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class IndexHandler implements Route {
    StorageInterface db;

    public IndexHandler(StorageInterface db) {
        this.db = db;
    }
    
    String bodyHead = "<!DOCTYPE html>\n"
    		+ "<html>\n"
    		+ "<head>\n"
    		+ "    <title>Sample File</title>\n"
    		+ "</head>\n"
    		+ "<body>\n"
    		+ "<h1>Welcome ";
    
    String bodyTail = "</h1>"
    		+ "</body>\n"
    		+ "</html>";
    
    @Override
    public String handle(Request req, Response resp) throws HaltException {
    	Session session = req.session();
    	String user = session.attribute("user");
    	String password = session.attribute("password");
    	if (user.equals(null) || password.equals(null)) {
    		resp.redirect("/login-form.html");
    	} else {
    		resp.status(200);
    		resp.body(bodyHead + user + bodyTail);
    	}

        return bodyHead + user + bodyTail;
    }
}