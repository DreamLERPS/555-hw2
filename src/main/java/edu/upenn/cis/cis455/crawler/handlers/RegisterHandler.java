package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegisterHandler implements Route {
	final static Logger logger = LogManager.getLogger(RegisterHandler.class);
	
    StorageInterface db;

    public RegisterHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
    	logger.debug("Trying to Register");
        String user = req.queryParams("username");
        String pass = req.queryParams("password");
        if (db.getSessionForUser(user, pass)) {
            Session session = req.session();

            session.attribute("user", user);
            session.attribute("password", pass);
            resp.redirect("/index.html");
        } else {
        	logger.debug("Adding new User");
        	db.addUser(user, pass);
        	Session session = req.session();

            session.attribute("user", user);
            session.attribute("password", pass);
            session.maxInactiveInterval(300);
        	resp.redirect("/index.html");
        }

        return "";
    }
}