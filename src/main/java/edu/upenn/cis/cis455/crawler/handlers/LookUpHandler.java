package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;

public class LookUpHandler implements Route {

	StorageInterface db;

    public LookUpHandler(StorageInterface db) {
        this.db = db;
    }
	@Override
	public Object handle(Request request, Response response) throws Exception {
		// TODO Auto-generated method stub
		String id = request.queryParams("url");
		String doc = db.getDocument((new URLInfo(id)).toString());
		if (doc == null) {
			response.status(404);
			response.body("");
			return "";
		} else {
			response.status(200);
			response.body(doc);
			return doc;
		}
	}

}
