package edu.upenn.cis.cis455;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RunAllTests extends TestCase 
{
//  public static Test suite() 
//  {
//    try {
//      Class[]  testClasses = {
//        /* TODO: Add the names of your unit test classes here */
//        // Class.forName("your.class.name.here") 
//      };   
//      
//      return new TestSuite(testClasses);
//    } catch(Exception e){
//      e.printStackTrace();
//    } 
//    
//    return null;
//  }
  
  @Test
  public void crawltest1() throws IOException {
	  Crawler c = new Crawler("", null, 1, 10);
	  c.getLinks(new URLInfo("https://crawltest.cis.upenn.edu/"));
	  assertTrue(c.queue.contains("https://crawltest.cis.upenn.edu/nytimes/"));
	  assertTrue(c.queue.contains("https://crawltest.cis.upenn.edu/bbc/"));
  }
  @Test
  public void crawltest2() throws IOException {
	  Crawler c = new Crawler("", null, 1, 10);
	  c.getLinks(new URLInfo("https://crawltest.cis.upenn.edu/"));
	  assertFalse(c.robotCheck("https://crawltest.cis.upenn.edu/marie/private/middleeast.xml"));
  }
  @Test
  public void dbtest1() {
	  StorageImpl s = new StorageImpl("./databse");
	  assertTrue(s.getDocument("ashdjfgskdfbjksa") == null);
  }
  @Test
  public void dbtest2() {
	  StorageImpl s = new StorageImpl("./databse");
	  assertFalse(s.getSessionForUser("ashdjfgskdfbjksa", "sbdjf"));
  }
}
